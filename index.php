<?php
    #La Vista que invoca las vistas que se desean mostrar.
    require "Controlador/Ayuntamiento.php";
    #Variables de uso.
    $class;
    $funpag;
    if (!isset($_GET['fp']))
    {
        #Instanciando nuestra clase de trabajo.
        $class = new Controlador\Ayuntamiento();
        #Obtener la funcion de la pagina.
        $funpag = $class->principal();
    }
    else
    {
        #Condici�n de optener la funci�n de la pagina amostrar.
        $funpag = isset($_GET['fp']) ? $_GET['fp'] : 'index';
        #Instanciando nuestro objeto.
        $class = new Controlador\Ayuntamiento();
        #Llamando la funcion de pagina para mostrar.
        call_user_func(array($class, $funpag));
    }
?>