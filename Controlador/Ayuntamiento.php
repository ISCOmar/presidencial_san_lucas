<?php
namespace Controlador;
class Ayuntamiento{
    public function index()
    { 
        include "Vista/index/index.phtml"; 
    }
/*----------------------------------------------------------------------------------
                    Funciones de pie de páginas
----------------------------------------------------------------------------------*/
    public function principal(){
        include_once "Vista/header.phtml";
        include_once "Vista/principal/principal.phtml";
        include_once "Vista/footer.phtml";
    }
    public function principal2(){
        include_once "Vista/header.phtml";
        include_once "Vista/principal/principal2.phtml";
        include_once "Vista/footer.phtml";
    }
    public function principal3(){
        include_once "Vista/header.phtml";
        include_once "Vista/principal/principal3.phtml";
        include_once "Vista/footer.phtml";
    }
    public function principal4(){
        include_once "Vista/header.phtml";
        include_once "Vista/principal/principal4.phtml";
        include_once "Vista/footer.phtml";
    }
/*----------------------------------------------------------------------------------
                    Funciones del menú principal
----------------------------------------------------------------------------------*/
    public function presidencia()
    {
        include "Vista/header.phtml";
        include "Vista/presidencia/presidencia.phtml";
        include "Vista/footer.phtml";
    }
    public function regidores()
    {
        include "Vista/header.phtml";
        include "Vista/regidores/regidores.phtml";
        include "Vista/footer.phtml";
    }
    public function comisiones()
    {
        include "Vista/header.phtml";
        include "Vista/comisiones/comisiones.phtml";
        include "Vista/footer.phtml";
    }
    public function funcionarios()
    {
        include "Vista/header.phtml";
        include "Vista/funcionarios/funcionarios.phtml";
        include "Vista/footer.phtml";
    }
    public function turismo()
    {
        include "Vista/header.phtml";
        include "Vista/turismo/turismo.phtml";
        include "Vista/footer.phtml";
    }
    public function dependencia()
    {
        include "Vista/header.phtml";
        include "Vista/dependencia/dependencia.phtml";
        include "Vista/footer.phtml";
    }
    public function search()
    {
        include "Vista/header.phtml";
        include "Vista/search/search.phtml";
        include "Vista/footer.phtml";
    }
/*----------------------------------------------------------------------------------
                    Funciones del submenu principal
----------------------------------------------------------------------------------*/
    public function transparencia()
    {
        include "Vista/header.phtml";
        include "Vista/transparencia/transparencia.phtml";
        include "Vista/footer.phtml";
    }
    public function articulo_35()
    {
        include "Vista/header.phtml";
        include "Vista/transparencia/articulo_35.phtml";
        include "Vista/footer.phtml";
    }
    public function articulo_36()
    {
        include "Vista/header.phtml";
        include "Vista/transparencia/articulo_36.phtml";
        include "Vista/footer.phtml";
    }
    public function tramites()
    {
        include "Vista/header.phtml";
        include "Vista/tramites/tramites.phtml";
        include "Vista/footer.phtml";
    }
    public function reporte()
    {
        include "Vista/header.phtml";
        include "Vista/reporte/reporte.phtml";
        include "Vista/footer.phtml";
    }
    public function galeria()
    {
        include "Vista/header.phtml";
        include "Vista/galeria/galeria.phtml";
        include "Vista/footer.phtml";
    }
    public function obra()
    {
        include "Vista/header.phtml";
        include "Vista/galeria/obra.phtml";
        include "Vista/footer.phtml";
    }
/*----------------------------------------------------------------------------------
                    Funciones del menú de areas
----------------------------------------------------------------------------------*/
     public function liga_municipal(){
        include "Vista/header.phtml";
        include "Vista/liga_municipal/liga_municipal.phtml";
        include "Vista/footer.phtml";
    }
    public function obras(){
        include "Vista/header.phtml";
        include "Vista/obras/obras.phtml";
        include "Vista/footer.phtml";
    }
    public function atencion(){
        include "Vista/header.phtml";
        include "Vista/atencion/atencion.phtml";
        include "Vista/footer.phtml";
    }
    public function juventud(){
        include "Vista/header.phtml";
        include "Vista/juventud/juventud.phtml";
        include "Vista/footer.phtml";
    }
    public function salud(){
        include "Vista/header.phtml";
        include "Vista/salud/salud.phtml";
        include "Vista/footer.phtml";
    }
    public function desarrollo(){
        include "Vista/header.phtml";
        include "Vista/desarrollo/desarrollo.phtml";
        include "Vista/footer.phtml";
    }
    //end Areas
    public function videos()
    {
        include "Vista/header.phtml";
        include "Vista/videos/videos.phtml";
        include "Vista/footer.phtml";
    }
    #Principal Submenu N°:2
    public function historia()
    {
        include "Vista/header.phtml";
        include "Vista/historia/historia.phtml";
        include "Vista/footer.phtml";  
    }
    public function estadisticas()
    {
        include "Vista/header.phtml";
        include "Vista/estadisticas/estadisticas.phtml";
        include "Vista/footer.phtml";
    }
    public function datos()
    {
        include "Vista/header.phtml";
        include "Vista/datos/datos.phtml";
        include "Vista/footer.phtml";  
    }
    public function mapa()
    {
        include "Vista/header.phtml";
        include "Vista/mapa/mapa.phtml";
        include "Vista/footer.phtml";  
    }
    public function biografia()
    {
        include "Vista/header.phtml";
        include "Vista/biografia/biografia.phtml";
        include "Vista/footer.phtml";  
    }
    public function escuelas()
    {
        include "Vista/header.phtml";
        include "Vista/escuelas/escuelas.phtml";
        include "Vista/footer.phtml";  
    }
    public function codigos()
    {
        include "Vista/header.phtml";
        include "Vista/codigos/codigos.phtml";
        include "Vista/footer.phtml";  
    }
    public function localidades()
    {
        include "Vista/header.phtml";
        include "Vista/localidades/localidades.phtml";
        include "Vista/footer.phtml";  
    }
    public function directorio()
    {
        include "Vista/header.phtml";
        include "Vista/directorio/directorio.phtml";
        include "Vista/footer.phtml";  
    }
    #Cargos
    public function cargo_presidente()
    {
        include "Vista/header.phtml";
        include "Vista/cargo/cargo_presidente.phtml";
        include "Vista/footer.phtml";
    }
    public function cargo_sindico()
    {
        include "Vista/header.phtml";
        include "Vista/cargo/cargo_sindico.phtml";
        include "Vista/footer.phtml";
    }
    public function cargo_tesoreria()
    {
        include "Vista/header.phtml";
        include "Vista/cargo/cargo_tesoreria.phtml";
        include "Vista/footer.phtml";
    }
    public function cargo_desarrollo_rural()
    {
        include "Vista/header.phtml";
        include "Vista/cargo/cargo_desarrollo_rural.phtml";
        include "Vista/footer.phtml";
    }
    public function cargo_atencion_migrante()
    {
        include "Vista/header.phtml";
        include "Vista/cargo/cargo_atencion_migrante.phtml";
        include "Vista/footer.phtml";
    }
    public function cargo_comunicacion_social()
    {
        include "Vista/header.phtml";
        include "Vista/cargo/cargo_comunicacion_social.phtml";
        include "Vista/footer.phtml";
    }
    public function cargo_secretario()
    {
        include "Vista/header.phtml";
        include "Vista/cargo/cargo_secretario.phtml";
        include "Vista/footer.phtml";
    }
    public function cargo_contralora()
    {
        include "Vista/header.phtml";
        include "Vista/cargo/cargo_contralora.phtml";
        include "Vista/footer.phtml";
    }
    
    public function cargo_obras_publicas()
    {
        include "Vista/header.phtml";
        include "Vista/cargo/cargo_obras_publicas.phtml";
        include "Vista/footer.phtml";
    }
    public function cargo_urbanismo()
    {
        include "Vista/header.phtml";
        include "Vista/cargo/cargo_urbanismo.phtml";
        include "Vista/footer.phtml";
    }
    public function cargo_oficial_mayor()
    {
        include "Vista/header.phtml";
        include "Vista/cargo/cargo_oficial_mayor.phtml";
        include "Vista/footer.phtml";
    }
    public function cargo_seguridad_publica()
    {
        include "Vista/header.phtml";
        include "Vista/cargo/cargo_seguridad_publica.phtml";
        include "Vista/footer.phtml";
    }
    public function cargo_instancia_de_mujer()
    {
        include "Vista/header.phtml";
        include "Vista/cargo/cargo_instancia_de_mujer.phtml";
        include "Vista/footer.phtml";
    }
/*----------------------------------------------------------------------------------
                    Funciones de Fracciones
----------------------------------------------------------------------------------*/
    public function fraccion1()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion1.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion2()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion2.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion3()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion3.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion4()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion4.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion5()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion5.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion6()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion6.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion7()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion7.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion8()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion8.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion9()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion9.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion10()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion10.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion11()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion11.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion12()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion12.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion13()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion13.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion14()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion14.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion15()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion15.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion16()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion16.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion17()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion17.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion18()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion18.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion19()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion19.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion20()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion20.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion21()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion21.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion22()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion22.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion23()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion23.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion24()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion24.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion25()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion25.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion26()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion26.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion27()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion27.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion28()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion28.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion29()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion29.phtml";
        include "Vista/footer.phtml";  
    }
    public function fraccion30()
    {
        include "Vista/header.phtml";
        include "Vista/fracciones/fraccion30.phtml";
        include "Vista/footer.phtml";  
    }
    //Cierre de fracciones
    public function enviar()
    {
        include "Vista/header.phtml";
        include "Vista/enviar/enviar.phtml";
        include "Vista/footer.phtml";  
    }
/*----------------------------------------------------------------------------------
                    Funciones del año 2016
----------------------------------------------------------------------------------*/
    public function agt(){
        include "Vista/header.phtml";
        include "Vista/2016/agt/agt.phtml";
        include "Vista/footer.phtml";
    }
    public function sep(){
        include "Vista/header.phtml";
        include "Vista/2016/sep/sep.phtml";
        include "Vista/footer.phtml";
    }

    public function oct(){
        include "Vista/header.phtml";
        include "Vista/2016/oct/oct.phtml";
        include "Vista/footer.phtml";
    }
    public function nov(){
        include "Vista/header.phtml";
        include "Vista/2016/nov/nov.phtml";
        include "Vista/footer.phtml";
    }
    public function dic(){
        include "Vista/header.phtml";
        include "Vista/2016/dic/dic.phtml";
        include "Vista/footer.phtml";
    }
/*----------------------------------------------------------------------------------
                    Funciones del año 2017
----------------------------------------------------------------------------------*/
    public function ene(){
        include "Vista/header.phtml";
        include "Vista/2017/ene/ene.phtml";
        include "Vista/footer.phtml";
    }
    public function feb(){
        include "Vista/header.phtml";
        include "Vista/2017/feb/feb.phtml";
        include "Vista/footer.phtml";
    }
    public function mar(){
        include "Vista/header.phtml";
        include "Vista/2017/mar/mar.phtml";
        include "Vista/footer.phtml";
    }
    public function abr(){
        include "Vista/header.phtml";
        include "Vista/2017/abr/abr.phtml";
        include "Vista/footer.phtml";
    }
    public function may(){
        include "Vista/header.phtml";
        include "Vista/2017/may/may.phtml";
        include "Vista/footer.phtml";
    }
    public function jun(){
        include "Vista/header.phtml";
        include "Vista/2017/jun/jun.phtml";
        include "Vista/footer.phtml";
    }
    public function jul(){
        include "Vista/header.phtml";
        include "Vista/2017/jul/jul.phtml";
        include "Vista/footer.phtml";
    }
    public function ago(){
        include "Vista/header.phtml";
        include "Vista/2017/ago/ago.phtml";
        include "Vista/footer.phtml";
    }
    public function sept(){
        include "Vista/header.phtml";
        include "Vista/2017/sep/sep.phtml";
        include "Vista/footer.phtml";
    }
    public function octu(){
        include "Vista/header.phtml";
        include "Vista/2017/oct/oct.phtml";
        include "Vista/footer.phtml";
    }
    public function novi(){
        include "Vista/header.phtml";
        include "Vista/2017/nov/nov.phtml";
        include "Vista/footer.phtml";
    }
    public function dici(){
        include "Vista/header.phtml";
        include "Vista/2017/dic/dic.phtml";
        include "Vista/footer.phtml";
    }
}